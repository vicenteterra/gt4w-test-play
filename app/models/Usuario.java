package models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Model;

@Entity(name = "usuario")
public class Usuario extends Model {
	/* Instaciando um finder do ebean para a entidade 'usuario'*/
	public static Finder<Long, Usuario> find = new Finder<Long, Usuario>(Usuario.class); 

	/**
	 * Método que busca um usuário na base por seu cpf
	 * @param cpf
	 * @return Usuario
	 */
	public static Usuario findByCpf(String cpf) {
		return find.where().eq("cpf", cpf).findUnique();
	}

	@Id
	private Integer id;
	@Column(name = "nome")
	private String nome;
	@Column(name = "cpf", unique = true)
	private String cpf;
	@Column(name = "bdate_unixtime")
	private Long bDateUnixtime;
	@Column(name = "peso")
	private Double peso;
	@Column(name = "uf_id")
	private Integer ufID;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public Long getbDateUnixtime() {
		return bDateUnixtime;
	}

	public void setbDateUnixtime(Long bDateUnixtime) {
		this.bDateUnixtime = bDateUnixtime;
	}

	public Double getPeso() {
		return peso;
	}

	public void setPeso(Double peso) {
		this.peso = peso;
	}

	public Integer getUfID() {
		return ufID;
	}

	public void setUfID(Integer ufID) {
		this.ufID = ufID;
	}

}
