package filters;

import play.filters.cors.CORSFilter;
import play.http.DefaultHttpFilters;

import javax.inject.Inject;

/* Classe de filtro para evitar erros de CORS nas requisições*/
public class Filters extends DefaultHttpFilters {
	@Inject
	public Filters(CORSFilter corsFilter) {
		super(corsFilter);
	}
}