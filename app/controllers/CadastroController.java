package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import models.Usuario;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

public class CadastroController extends Controller {
	/**
	 * Cadastro de usuario acessado pela url /usuario/new
	 * 
	 * @return
	 */
	public Result novoUsuario() {

		JsonNode json = request().body().asJson(); // Guardando o corpo da requisição em um json para manipular os dados
		Usuario novoUsuario = new Usuario();
		ObjectNode jsResp = Json.newObject();
		/*
		 * Criação de um objeto da classe Usuario com os dados passados no fomulário de
		 * cadastro no front-end
		 */
		novoUsuario.setNome(json.findValue("nome").asText());
		novoUsuario.setCpf(json.findValue("cpf").asText());
		novoUsuario.setPeso(json.findValue("peso") != null ? json.findValue("peso").asDouble() : 0);
		novoUsuario.setUfID(json.findValue("ufID") != null ? json.findValue("ufID").asInt() : 0);
		novoUsuario.setbDateUnixtime(json.findValue("bdateUnix") != null ? json.findValue("bdateUnix").asLong() : 0);
		try {
			if (Usuario.findByCpf(novoUsuario.getCpf()) == null) { // Check cpf duplicado
				novoUsuario.save();
				jsResp.put("status", 0);
				jsResp.put("message", "Usuário cadastrado com sucesso!");
			} else {
				jsResp.put("status", 1);
				jsResp.put("message", "Erro: Cpf já cadastrado!");
			}
		} catch (Exception e) {
			jsResp.put("status", 1);
			jsResp.put("message", "Erro no cadastro : Contate a equipe de desenvolvimento!");
			System.out.println(e.getMessage());
		}
		return ok(jsResp);
	}
}
