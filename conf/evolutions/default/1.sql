# --- Created by Ebean DDL
# To stop Ebean DDL generation, remove this comment and start using Evolutions

# --- !Ups

create table usuario (
  id                            integer auto_increment not null,
  nome                          varchar(255),
  cpf                           varchar(255),
  bdate_unixtime                bigint,
  peso                          double,
  uf_id                         integer,
  constraint uq_usuario_cpf unique (cpf),
  constraint pk_usuario primary key (id)
);


# --- !Downs

drop table if exists usuario;

