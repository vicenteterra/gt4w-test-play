name := """play-java"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava,PlayEbean)

libraryDependencies += "mysql" % "mysql-connector-java" % "5.1.36"

scalaVersion := "2.11.7"
libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  javaWs,
  filters
)


fork in run := true